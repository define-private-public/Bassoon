// License:     APL 2.0
// Author:      Benjamin N. Summerton <https://16bpp.net>

namespace libsndfileSharp
{
    public enum Seek
    {
        Set,
        Cur,
        End
    }
}
